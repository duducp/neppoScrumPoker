import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {JwtHelper} from "angular2-jwt";

import {AuthService} from '../providers/auth/authService';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string;
  activePage: any;
  jwtHelper = new JwtHelper();
  userLogged: any = {};

  pages: Array<{ title: string, icon?: string, component: string }>;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public authService: AuthService) {
    this.initializeApp();
    this.pages = [
      {title: 'Projetos', icon: 'home', component: 'ProjetosPage'},
    ];
    this.activePage = this.pages[0];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();

      this.authService.isTokenExpired().then((result) => {
        if (!result) {
          this.getUserLogged();
          this.nav.setRoot('ProjetosPage');
        }
      }, (err) => {
        this.nav.setRoot('LoginPage');
      });

      this.hideSplashScreen();
    });
  }

  getUserLogged() {
    this.authService.getUserLogged()
      .then(data => {
        this.userLogged = data;
      })
      .catch(err => {
        console.log({erro: err});
      })
  }

  hideSplashScreen() {
    setTimeout(() => {
      this.splashScreen.hide();
    }, 100);
  }

  openPage(page) {
    this.nav.setRoot(page.component);
    this.activePage = page;
  }

  checkActive(page) {
    return page == this.activePage;
  }

  logout() {
    this.authService.logout();
    this.nav.setRoot('LoginPage');
  }
}
