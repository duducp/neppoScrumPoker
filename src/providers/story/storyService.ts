import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Storage} from '@ionic/storage';

import {AppConfig} from './../app-config.provider';

@Injectable()
export class StoryService {
    private token: any;

    constructor(public http: Http, public storage: Storage, public appConfig: AppConfig) {
    }

	getAllStoryProject(idProject) {
		return new Promise((resolve, reject) => {

			//Load token if exists
			this.storage.get('token').then((value) => {
				this.token = value;

				let headers = new Headers();
				headers.append('Authorization', this.token);

                this.http.get(this.appConfig.apiEndpoint + 'story/' + idProject, {headers: headers})
					.map(res => res.json())
					.subscribe(data => {
						resolve(data);
					}, (err) => {
						reject(err);
					});

			});
		});
	}

    getAllStorysVoted(parametros) {
        return new Promise((resolve, reject) => {
            this.storage.get('token').then((value) => {
                this.token = value;

                let headers = new Headers();
                headers.append('Authorization', this.token);

                this.http.get(this.appConfig.apiEndpoint + 'storys-voted/' + parametros, {headers: headers})
                    .map(res => res.json())
                    .subscribe(data => {
                        resolve(data);
                    }, (err) => {
                        reject(err);
                    });

            });
        });
    }

    getStoryProject(idProject, idStory) {
        return new Promise((resolve, reject) => {

            //Load token if exists
            this.storage.get('token').then((value) => {
                this.token = value;

                let headers = new Headers();
                headers.append('Authorization', this.token);

                this.http.get(this.appConfig.apiEndpoint + 'story/' + idProject + '/' + idStory, {headers: headers})
                    .map(res => res.json())
                    .subscribe(data => {
                        resolve(data);
                    }, (err) => {
                        reject(err);
                    });

            });
        });
    }

	saveStory(data, idProject) {
		return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
				this.token = value;

				let headers = new Headers();
				headers.append('Content-Type', 'application/json');
				headers.append('Authorization', this.token);

                this.http.post(this.appConfig.apiEndpoint + 'story/' + idProject, JSON.stringify(data), {headers: headers})
					.subscribe(res => {
						let data = res.json();
						resolve(data);

					}, (err) => {
						reject(err);
					});
			});
		});
	}

	saveStoryVoting(note: object, idProject: string, idStory: string) {
		return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
				this.token = value;

				let headers = new Headers();
				headers.append('Content-Type', 'application/json');
				headers.append('Authorization', this.token);

                this.http.post(this.appConfig.apiEndpoint + 'storys-voted/' + idProject + '/' + idStory, JSON.stringify(note), {headers: headers})
					.subscribe(res => {
						let data = res.json();
						resolve(data);

					}, (err) => {
						reject(err);
					});
			});
		});
	}

	updateStory(data: object, idProject: string, idStory: string) {
		return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
				this.token = value;

				let headers = new Headers();
				headers.append('Content-Type', 'application/json');
				headers.append('Authorization', this.token);

                this.http.put(this.appConfig.apiEndpoint + 'story/' + idProject + '/' + idStory, JSON.stringify(data), {headers: headers})
					.subscribe(res => {
						let data = res.json();
						resolve(data);

					}, (err) => {
						reject(err);
					});
			});
		});
	}

	deleteStory(idProject: string, idStory: string) {
		return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
				this.token = value;

				let headers = new Headers();
				headers.append('Content-Type', 'application/json');
				headers.append('Authorization', this.token);

                this.http.delete(this.appConfig.apiEndpoint + 'story/' + idProject + '/' + idStory, {headers: headers})
					.subscribe(res => {
						let data = res.json();
						resolve(data);

					}, (err) => {
						reject(err);
					});
			});
		});
	}

	deleteAllVotingStoryProject(idProject: string, idStory: string) {
		return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
				this.token = value;

				let headers = new Headers();
				headers.append('Content-Type', 'application/json');
				headers.append('Authorization', this.token);

                this.http.delete(this.appConfig.apiEndpoint + 'storys-voted/' + idProject + '/' + idStory, {headers: headers})
					.subscribe(res => {
						let data = res.json();
						resolve(data);

					}, (err) => {
						reject(err);
					});
			});
		});
	}
}
