import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Storage} from '@ionic/storage';

import {AppConfig} from './../app-config.provider';

@Injectable()
export class UsersService {
	public token: any;

    constructor(public http: Http, public storage: Storage, public appConfig: AppConfig) {
    }

	getAllUsers() {
		return new Promise((resolve, reject) => {

			//Load token if exists
			this.storage.get('token').then((value) => {
				this.token = value;

				let headers = new Headers();
				headers.append('Authorization', this.token);

                this.http.get(this.appConfig.apiEndpoint + 'user/?orderBy=name&sort=asc&select=name', {headers: headers})
					.map(res => res.json())
					.subscribe(data => {
						resolve(data);
					}, (err) => {
						reject(err);
					});

			});
		});
	}

}
