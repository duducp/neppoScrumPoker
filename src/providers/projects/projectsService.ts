import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Storage} from '@ionic/storage';

import {AppConfig} from './../app-config.provider';

@Injectable()
export class ProjectsService {
    private token: any;

    constructor(public http: Http, public storage: Storage, public appConfig: AppConfig) {
    }

    getAllProjects() {
		return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
				this.token = value;

				let headers = new Headers();
				headers.append('Authorization', this.token);

                this.http.get(this.appConfig.apiEndpoint + 'project?userTeam=true&sort=asc&orderBy=date_end start_in_date date_start', {headers: headers})
					.map(res => res.json())
					.subscribe(data => {
						resolve(data);
					}, (err) => {
                        console.log("Falha ao conectar com o servidor.");
                        reject(err);
					});

            });
        });
    }

	getProject(idProject: string) {
        return new Promise((resolve, reject) => {
            this.storage.get('token').then((value) => {
                this.token = value;

                let headers = new Headers();
                headers.append('Authorization', this.token);

				this.http.get(this.appConfig.apiEndpoint + 'project/' + idProject + '/?userTeam=true', {headers: headers})
                    .map(res => res.json())
                    .subscribe(data => {
                        resolve(data);
                    }, (err) => {
                        console.log("Falha ao conectar com o servidor.");
                        reject(err);
                    });

			});
		});
	}

	saveProject(data) {
		return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
				this.token = value;

				let headers = new Headers();
				headers.append('Content-Type', 'application/json');
				headers.append('Authorization', this.token);

                this.http.post(this.appConfig.apiEndpoint + 'project', JSON.stringify(data), {headers: headers})
					.subscribe(res => {
						let data = res.json();
						resolve(data);

					}, (err) => {
						reject(err);
					});
			});
		});
	}

    updateProject(data: object, idProject: string) {
		return new Promise((resolve, reject) => {
			this.storage.get('token').then((value) => {
				this.token = value;

				let headers = new Headers();
				headers.append('Content-Type', 'application/json');
				headers.append('Authorization', this.token);

                this.http.put(this.appConfig.apiEndpoint + 'project/' + idProject, JSON.stringify(data), {headers: headers})
					.subscribe(res => {
						let data = res.json();
						resolve(data);

					}, (err) => {
						reject(err);
					});
			});
		});
	}

	deleteProject(idProject: string) {
        return new Promise((resolve, reject) => {
            this.storage.get('token').then((value) => {
                this.token = value;

                let headers = new Headers();
                headers.append('Content-Type', 'application/json');
                headers.append('Authorization', this.token);

				this.http.delete(this.appConfig.apiEndpoint + 'project/' + idProject, {headers: headers})
                    .subscribe(res => {
                        let data = res.json();
                        resolve(data);

                    }, (err) => {
                        reject(err);
                    });
            });
        });
    }
}
