import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Storage} from '@ionic/storage';
import {JwtHelper} from "angular2-jwt";

import {AppConfig} from './../app-config.provider';

@Injectable()
export class AuthService {
    private jwtHelper = new JwtHelper();

    constructor(public http: Http, public storage: Storage, public appConfig: AppConfig) {
    }

  isTokenExpired() {
    return new Promise((resolve, reject) => {
      this.storage.get('token')
        .then((value) => {
          resolve(this.jwtHelper.isTokenExpired(value));
        })
        .catch(function (err) {
          reject({erro: err});
        });
    });
  }

  login(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

        this.http.post(this.appConfig.apiEndpoint + 'auth/login', JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          let data = res.json();
          this.storage.set('token', data.token);
          this.storage.set('profile', data.user);
          resolve(data.user);

        }, (err) => {
          reject(err);
        });
    });
  }

  getUserLogged() {
      return new Promise((resolve, reject) => {
          this.storage.get('profile')
              .then((value) => {
                  resolve(value);
              })
              .catch(function (err) {
                  reject(err);
              });
      });
  }

	logout(): void {
		this.storage.remove('token');
		this.storage.remove('profile');
	};
}
