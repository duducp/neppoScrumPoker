import {NgModule} from '@angular/core';

import {AppConfig} from './app-config.provider';
import {AuthService} from './auth/authService';
import {UsersService} from './users/usersService';
import {ProjectsService} from './projects/projectsService';
import {StoryService} from './story/storyService';

@NgModule({
    providers: [
        AppConfig,
        AuthService,
        UsersService,
        ProjectsService,
        StoryService,
    ]
})
export class ServicesModule {
}