export class AppConfig {
    private url: string;
    public apiEndpoint: string;
    public socketEndpoint: string;

    constructor() {
        this.url = 'http://18.231.77.99/';
        //this.url = 'http://localhost:9000/';
        this.apiEndpoint = this.url + 'api/v1/';
        this.socketEndpoint = this.url;
    }
}