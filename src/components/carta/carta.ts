import {Component, Input} from '@angular/core';

@Component({
	selector: 'carta',
	templateUrl: 'carta.html'
})
export class CartaComponent {
	@Input() typeCard: string;
	@Input() textDesabled: string;
	@Input() nameUser: string;
	@Input() pictureUser: string;
	@Input() departmentUser: string;
	@Input() textCard: string;

	constructor() {
	}
}
