import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CartaComponent} from './carta';

@NgModule({
    declarations: [
        CartaComponent,
    ],
    imports: [
        IonicPageModule.forChild(CartaComponent),
    ],
    exports: [
        CartaComponent
    ]
})
export class CartaComponentModule {
}
