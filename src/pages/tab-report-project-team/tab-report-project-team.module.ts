import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {TabReportProjectTeamPage} from './tab-report-project-team';

@NgModule({
    declarations: [
        TabReportProjectTeamPage,
    ],
    imports: [
        IonicPageModule.forChild(TabReportProjectTeamPage),
    ],
    exports: [
        TabReportProjectTeamPage
    ]
})
export class TabReportProjectTeamPageModule {
}
