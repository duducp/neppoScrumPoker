import {Component} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    LoadingController,
    AlertController,
    App,
    ItemSliding,
    ModalController
} from 'ionic-angular';


import {StoryService} from "../../providers/story/storyService";

@IonicPage()
@Component({
    selector: 'page-tab-report-project-team',
    templateUrl: 'tab-report-project-team.html',
})
export class TabReportProjectTeamPage {
    paramsProject: any;
    users: any[] = [];

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public appCtrl: App,
                public loadingCtrl: LoadingController,
                public modalCtrl: ModalController,
                public alertCtrl: AlertController,
                public storyService: StoryService) {
        this.paramsProject = navParams.data;
    }

    ionViewDidLoad() {
        this.getAllStorysVoted();
    }

    getAllStorysVoted() {
        let loader = this.loadingCtrl.create({
            content: "Gerando relatório..."
        });
        loader.present();

        this.storyService.getAllStorysVoted(this.paramsProject._id + '/?orderUsers=true')
            .then(res => {
                let data: any = res;
                this.users = data;

                loader.dismissAll();
            })
            .catch((err) => {
                loader.dismissAll();
            });
    }

    dismiss() {
        this.appCtrl.getRootNav().pop();
    }

    doRefresh(refresher) {
        setTimeout(() => {
            this.getAllStorysVoted();
            refresher.complete();
        }, 2000);
    }

    summaryStory(story, slidingItem: ItemSliding) {
        let modal = this.modalCtrl.create('VotingProjectReportStoryPage', {
            project: this.paramsProject,
            story: story
        });
        modal.present();
        modal.onDidDismiss(() => {
            slidingItem.close();
        });
    }

    viewDescriptionStory(story, slidingItem: ItemSliding) {
        let alert = this.alertCtrl.create({
            title: story.title,
            subTitle: story.description,
            buttons: ['Ok']
        });
        alert.present();
        alert.onDidDismiss(() => {
            slidingItem.close();
        });
    }

    viewDescriptionProject() {
        let alert = this.alertCtrl.create({
            title: this.paramsProject.title,
            subTitle: this.paramsProject.description,
            buttons: ['Ok']
        });
        alert.present();
    }
}