import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';

import {VotacaoProjetoEstoriasPage} from './votacao-projeto-estorias';

@NgModule({
    declarations: [VotacaoProjetoEstoriasPage],
    imports: [
        IonicPageModule.forChild(VotacaoProjetoEstoriasPage),
    ]
})
export class VotacaoProjetoEstoriasPageModule {
}