import {Component} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    LoadingController,
    ToastController,
    ViewController,
    AlertController,
    ModalController
} from 'ionic-angular';

import {StoryService} from "../../providers/story/storyService";
import {ProjectsService} from './../../providers/projects/projectsService';

@IonicPage()
@Component({
    selector: 'page-votacao-projeto-estorias',
    templateUrl: 'votacao-projeto-estorias.html'
})
export class VotacaoProjetoEstoriasPage {
    paramsProject: any;
    storys: any[] = [];
    idStorySelected: string;
    dataUserLogged: {};

    constructor(public navCtrl: NavController,
                public loadingCtrl: LoadingController,
                public navParams: NavParams,
                public toastCtrl: ToastController,
                public viewCtrl: ViewController,
                public alertCtrl: AlertController,
                public modalCtrl: ModalController,
                public storyService: StoryService,
                public projectService: ProjectsService) {

        // Pega os dados do projeto
        this.paramsProject = navParams.get('project');
        this.dataUserLogged = navParams.get('userLogged');
    }

    ionViewDidLoad() {
        this.getAllStories();
    }

    getAllStories() {
        let loader = this.loadingCtrl.create({
            content: "Carregando estórias..."
        });
        loader.present();

        this.storyService.getAllStoryProject(this.paramsProject._id)
            .then(res => {
                let s: any = res;
                this.storys = s;
                loader.dismissAll();
            })
            .catch((err) => {
                let body = JSON.parse(err._body);
                let message = body.message;
                this.showToast(message, 3000);
                this.storys = [];
                loader.dismissAll();
            });
    }

    selectStory(idStory) {
        this.idStorySelected = idStory;
    }

    viewDescriptionStory(story) {
        let alert = this.alertCtrl.create({
            title: story.title,
            subTitle: story.description,
            buttons: ['OK']
        });
        alert.present();
    }

    insertStory() {
        let modal = this.modalCtrl.create('CadastroEstoriaPage', {idProject: this.paramsProject._id});
		modal.present();
		modal.onDidDismiss((res) => {
            if (res) {
                this.storys.push(res);
            }
		});
	}

    startVoting() {
        if (!this.idStorySelected) {
            this.showToast("Você deve selecionar uma estória para continuar", 3000);
        } else {
            let confirm = this.alertCtrl.create({
                title: 'Iniciar votação',
                message: 'Deseja iniciar uma nova votação?',
                buttons: [
                    {text: 'Não'},
                    {
                        text: 'Sim',
                        handler: () => {
                            let loader = this.loadingCtrl.create({
                                content: "Iniciando votação..."
                            });
                            loader.present();

                            this.projectService.updateProject({current_story: this.idStorySelected}, this.paramsProject._id).then(() => {
                                
                                this.storyService.getStoryProject(this.paramsProject._id, this.idStorySelected)
                                .then(res => {
                                    loader.dismissAll();
                                    this.viewCtrl.dismiss({story: res, total_story: this.storys.length});
                                })
                                .catch((err) => {
                                    let body = JSON.parse(err._body);
                                    let message = body.message;
                                    this.showToast(message, 3000);
                                    loader.dismissAll();
                                });

                            }, (err) => {
                                let body = JSON.parse(err._body);
                                let message = body.message;
                                this.showToast(message, 3000);
                                loader.dismissAll();
                            });
                        }
                    }
                ]
            });
            confirm.present();
        }
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    doRefresh(refresher) {
        setTimeout(() => {
            this.getAllStories();
            refresher.complete();
        }, 2000);
    }

    showToast(message: string, duration?: number) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            showCloseButton: true,
            closeButtonText: "Ok"
        });
        toast.present();
    }

}
