import {Component} from '@angular/core';
import {
    IonicPage,
    NavController,
    MenuController,
    ToastController,
    LoadingController,
    AlertController,
    ViewController,
    NavParams
} from 'ionic-angular';
import {Validators, FormGroup, FormBuilder} from "@angular/forms";
import * as moment from 'moment';

import {UsersService} from "../../providers/users/usersService";
import {ProjectsService} from "../../providers/projects/projectsService";

@IonicPage()
@Component({
	selector: 'page-cadastro-projeto',
	templateUrl: 'cadastro-projeto.html',
	providers: [UsersService, ProjectsService]
})
export class CadastroProjetoPage {
    //Variáveis
    loading: any;
    form: FormGroup;

    textTitlePage: string = "Novo Projeto";
    textSubmit: string = "Cadastrar";
    type: string;
    project: any;

    cartas = [];
    cardsSelect: any[];
    allUsers: {};
    myDate: string;
    
    dataUserLogged: any;

	constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public fb: FormBuilder,
                public menuCtrl: MenuController,
                public toastCtrl: ToastController,
                public loadingCtrl: LoadingController,
                public usersService: UsersService,
                public alertCtrl: AlertController,
                public viewCtrl: ViewController,
                public projectsService: ProjectsService) {
		menuCtrl.enable(false);

        // Define o idioma do moment
        moment.locale('pt-BR');
        this.myDate = moment().format();

        // Cartas iniciais do projeto
        this.cartas = [0, 0.5, 1, 2, 3, 5, 8, 13, 20, 40, 100, "∞", "☕", "?"];
        this.cardsSelect = [0, 0.5, 1, 2, 3, 5, 8, 13, 20, 40, 100, "∞", "☕", "?"];

        // Válida os campos no form
		this.form = fb.group({
			'title': [null, Validators.required],
			'description': [null, Validators.required],
            'cards': [this.cardsSelect],
			'team': [null, Validators.required],
			'date_start': [null, Validators.required]
		});

        this.type = this.navParams.get('type');
        this.dataUserLogged = this.navParams.get('userLogged');
	}

	ionViewDidLoad() {
		this.getAllUsers();
        this.checkTypeProject();
	}

    checkTypeProject() {
        if (this.type === 'edit') {
            this.project = this.navParams.get('project');

            // Informa ao validator quais os dados que os inputs tem
            this.form.setValue({
                title: this.project.title,
                description: this.project.description,
                cards: this.project.cards,
                team: this.project.team,
                date_start: this.project.date_start,
            });

            this.cartas = this.project.cards;
            this.cardsSelect = this.project.cards;

            this.textTitlePage = "Edição de Projeto";
            this.textSubmit = "Atualizar";
        }
    }

	getAllUsers() {
		this.usersService.getAllUsers()
			.then(res => {
				this.allUsers = res;
			})
			.catch((err) => {
				let body = JSON.parse(err._body);
				let message = body.message;
				this.showToast(message, 3000);
			});
	}

    addCard() {
        let alert = this.alertCtrl.create({
            title: 'Adicionar Carta',
            inputs: [{
                name: 'valor',
                placeholder: 'Informe um valor',
                type: 'number'
            }],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                },
                {
                    text: 'Adicionar',
                    handler: data => {
                        this.cartas.push(data.valor);
                        this.cardsSelect.push(data.valor);
                    }
                }
            ]
        });
        alert.present();
    }

    selectCard(card, index, event) {
        if (event.checked == false) {
            for (let i = 0; i < this.cardsSelect.length; i++) {
                if (this.cardsSelect[i] == card)
                    this.cardsSelect.splice(i, 1);
            }
        } else {
            this.cardsSelect.splice(index, 0, card);
        }
    }

    infoCard() {
        let alert = this.alertCtrl.create({
            title: 'Detalhes das Cartas',
            subTitle: `<b>?</b>: O membro não se sente confiante para atribuir um valor a tarefa.<br>
						<b>0</b>: A tarefa é absolutamente desnecessária e deveria ser descartada.<br>
						<b>0.5</b>: A tarefa necessita de uma pequeno esforço para ser concluída.<br>
						<b>∞</b>: A tarefa é extremamente importante.<br>
						☕: Uma pausa para refletir antes de tomar a decisão.<br>
						- Demais números são a representação arbitrária do sentimento dos membros, quanto ao esforço necessário, para a realização da tarefa.`,
            buttons: ['Ok']
        });
        alert.present();
    }

    submit() {
		this.showLoader();

        if (this.type === 'edit') {
            this.projectsService.updateProject(this.form.value, this.project._id).then((result) => {
                let data: any = result;
                this.loading.dismiss();
                this.showToast(data.message, 3000);

                this.navCtrl.pop();

            }, (err) => {
                let body = JSON.parse(err._body);
                let message = body.message;
                this.showToast(message);
                this.loading.dismiss();
            });
        } else {
            this.projectsService.saveProject(this.form.value).then((result) => {
                let data: any = result;
                this.loading.dismiss();
                this.showToast(data.message, 3000);

                this.navCtrl.push('TabProjectPage', {project: data.project, indexTab: 1, userLogged: this.dataUserLogged});
                this.navCtrl.remove(1, 1);

            }, (err) => {
                let body = JSON.parse(err._body);
                let message = body.message;
                this.showToast(message);
                this.loading.dismiss();
            });
        }
	}

    dismiss() {
        this.viewCtrl.dismiss();
    }

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: 'Aguarde...'
		});

		this.loading.present();
	}

	showToast(message: string, duration?: number) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: duration,
			showCloseButton: true,
			closeButtonText: "Ok"
		});
		toast.present();
	}
}
