import {Component} from '@angular/core';
import {
    IonicPage,
    NavParams,
    NavController,
    ModalController,
    AlertController,
    MenuController,
    LoadingController,
    ToastController,
    PopoverController
} from 'ionic-angular';
import * as io from 'socket.io-client';
import * as moment from 'moment';

import {AppConfig} from '../../providers/app-config.provider';
import {StoryService} from "../../providers/story/storyService";
import {ProjectsService} from '../../providers/projects/projectsService';

@IonicPage()
@Component({
    selector: 'page-votacao-projeto',
    templateUrl: 'votacao-projeto.html'
})
export class VotacaoProjetoPage {

    // Variaveis
    dataUserLogged: any;
    paramsProject: any;
    idAdminProject: any;
    teamProject: any[] = [];
    teamProjectOnline: any[] = [];

    total_story: number;
    total_story_voted: number;
    storyCurrent: any = [];

    votingStarted: boolean = false;
    cardsTurn: boolean = false;
    usersWaitingVotingEnd: any[] = [];

    defaultMessageTextCardDesabled = "Aguardando início da votação...";
    textCardDesabled: string = this.defaultMessageTextCardDesabled;

    cardSelected: any;
    socket: any;

    // Cronometro
    stopCronometro: any;
    segundos: number;
    minutos: number;
    horas: number;

    // Notifications
    unreadNotifications: number = 0;
    dataNotifications: any[] = [];

    // Chat
    unreadChat: number = 0;
    dataChat: any[] = [];
    activeChat: boolean = true;

    constructor(public menuCtrl: MenuController,
                public navParams: NavParams,
                public modalCtrl: ModalController,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public toastCtrl: ToastController,
                public appConfig: AppConfig,
                public navCtrl: NavController,
                public popoverCtrl: PopoverController,
                public storyService: StoryService,
                public projectService: ProjectsService) {
        // Menu Toggle
        menuCtrl.enable(false);

        // Define o idioma do moment
        moment.locale('pt-BR');

        // Variaveis do projeto
        this.dataUserLogged = this.navParams.get('userLogged');
        let p = this.navParams.get('project');
        this.paramsProject = p.project;
        this.teamProject = this.paramsProject.team;
        this.idAdminProject = this.paramsProject.id_user[0]._id;
        this.storyCurrent = this.paramsProject.current_story[0] ? this.paramsProject.current_story[0] : [];
        this.total_story = p.total_story;
        this.total_story_voted = p.total_story_voted;

        // Conecta ao servidor RealTime
        this.socket = io.connect(this.appConfig.socketEndpoint + 'projects', {
            query:
            "idProject=" + this.paramsProject._id +
            "&nameUserConnected=" + this.dataUserLogged.name +
            "&idUserConnected=" + this.dataUserLogged._id +
            "&departmentUserConnected=" + this.dataUserLogged.department +
            "&userConnectedIsTeam=" + this.dataUserLogged.isTeam
        });

        // Adiciona ao array TeamProjectOnline o usuário que acabou de entrar, exceto para o mesmo
        this.socket.on("userConnected", (user) => {
            let obj = user;
            obj['typeCard'] = "desabled";

            if (!this.votingStarted) {
                this.teamProjectOnline.push(obj);
            } else {
                this.usersWaitingVotingEnd.push(obj);
            }
        });
        
        // Adiciona ao array TeamProjectOnline todos os usuários que estão online no projeto, somente para o usuário que entrou
        this.socket.on("userConnectedTeamOnline", (teamOnline, user, storyVotingStarted) => {
            for (let i = 0; i < teamOnline.length; i++) {
                let obj = teamOnline[i];
                if (!teamOnline[i].valueVotedCard) {
                    obj['typeCard'] = "desabled";
                } else {
                    obj['typeCard'] = "secret";
                }
                
                this.teamProjectOnline.push(obj);
            }

            // Verifica se o usuário pertence a equipe do projeto
            if (this.dataUserLogged.isTeam) {
                let obj = user;
                obj['typeCard'] = "desabled";

                if (storyVotingStarted) {
                    this.usersWaitingVotingEnd.push(obj);
                    this.textCardDesabled = "Aguardando usuário votar na estória";          
                } else {
                    this.teamProjectOnline.push(obj);
                }
            }
        });

        // Atualiza array TeamProject, quando o usuário é desconectado
        this.socket.on("userDesconnected", (idSocketDesconnect) => {
            let index = 0;
            for (const teamProject of this.teamProjectOnline) {
                if (teamProject.idSocket == idSocketDesconnect) {
                    this.teamProjectOnline.splice(index, 1);
                    break;
                }
                index = index + 1;
            }
        });

        // Comunica que a votação começou
        this.socket.on("startVoting", (res) => {
            this.total_story = res.total_story;
            this.total_story_voted = this.total_story_voted + 1;
            this.startVoting(res.story);
        });

        // Atualiza array TeamProject, quando o usuário é desconectado
        this.socket.on("votedCard", (data) => {
            let index = 0;
            for (const teamProject of this.teamProjectOnline) {
                if (teamProject.idSocket == data.idSocket) {
                    let obj = this.teamProjectOnline[index];
                    obj['valueVotedCard'] = data.voted;
                    obj['typeCard'] = 'secret';
                    break;
                }
                index = index + 1;
            }
        });

        // Reinicia a votação de uma estória
        this.socket.on("restartVoting", () => {
            for (let i = 0; i < this.teamProjectOnline.length; i++) {
                let obj = this.teamProjectOnline[i];
                delete obj['valueVotedCard'];
                obj['typeCard'] = 'desabled';
            }

            this.cardSelected = null;
            this.startVoting();
            this.showToast("A votação da estória foi reiniciada", 3000)
        })

        // Cancela a votação de uma estória
        this.socket.on("cancelVoting", () => {
            clearInterval(this.stopCronometro);
            this.segundos = 0;
            this.minutos = 0;
            this.horas = 0;

            this.desableCardsAndAddUsersWaitingVotingEnd();
            this.textCardDesabled = this.defaultMessageTextCardDesabled;
            this.cardsTurn = false;
            this.votingStarted = false;
            this.cardSelected = null;
            this.activeChat = true;
            this.storyCurrent = [];

            this.total_story_voted = this.total_story_voted - 1;

            if (this.total_story_voted !== this.total_story) {            
                let alert = this.alertCtrl.create({
                    title: "VOTAÇÃO CANCELADA",
                    subTitle: "A votação da estória atual foi cancelada",
                    buttons: [{text: 'Ok'}]
                });
                alert.present();
            }
        })

        // Vira a carta de todos os usuários
        this.socket.on("turnCards", () => {
            // Para o cronometro
            clearInterval(this.stopCronometro);

            for (let i = 0; i < this.teamProjectOnline.length; i++) {
                let obj = this.teamProjectOnline[i];
                if (this.teamProjectOnline[i].typeCard == "secret") {
                    obj['typeCard'] = 'active';
                }
            }

            // Fim da votação
            this.cardsTurn = true;
            this.textCardDesabled = "Usuário não participou da votação";
        });

         // Finaliza a votação de uma estória
        this.socket.on("finalCardStory", (card) => {
            this.desableCardsAndAddUsersWaitingVotingEnd();
            this.textCardDesabled = this.defaultMessageTextCardDesabled;
            this.cardsTurn = false;
            this.cardSelected = null;
            this.activeChat = true;

            this.showToast('Votação da estória concluída', 3000);

            let modal = this.modalCtrl.create('VotingProjectReportStoryPage', {
                project: this.paramsProject,
                story: this.storyCurrent,
                userLogged: this.dataUserLogged
            });
            modal.present();
            modal.onDidDismiss(() => {
                this.storyCurrent = [];
                this.votingStarted = false;
                
                if (this.total_story_voted == this.total_story) {
                    this.navCtrl.push('TabReportProjectPage', {project: this.paramsProject});
                    this.navCtrl.remove(0, 1);
                    this.navCtrl.insertPages(0, [{page: 'ProjetosPage'}])

                    this.showAlert('Votação Finalizada', 'A votação do projeto foi finalizada!')
                }
            });
        })

        // Informa sobre nova notificação
        this.socket.on("notification", (data) => {
            this.unreadNotifications = this.unreadNotifications + 1;
            this.dataNotifications.unshift(data);
        })

        // Informa sobre nova mensagem
        this.socket.on("messages", (data) => {
            this.unreadChat = this.unreadChat + 1;
            this.dataChat.push(data);
        })
    }

    cronometro() {
        if (this.segundos < 60) {
            this.segundos++;
        }

        if (this.segundos == 60) {
            this.segundos = 0;
            this.minutos++;
        }

        if (this.minutos == 60) {
            this.minutos = 0;
            this.horas++;
        }
    }

    // Executa antes da página ser mostrada
    ionViewDidEnter() {
        this.socket.emit('connected');
    }

    // Executa quando a página não esta mais ativa
    ionViewDidLeave() {
        this.socket.emit('userDesconnected');
    }

    openMenu() {
        let modal = this.modalCtrl.create('TabVotingProjectMenuPage', {
            project: this.paramsProject,
            userLogged: this.dataUserLogged,
            socket: this.socket,
            unreadNotifications: this.unreadNotifications,
            dataNotifications: this.dataNotifications
        });
        modal.present();
        modal.onDidDismiss((res) => {
            this.unreadNotifications = res;
        });
    }

    desableCardsAndAddUsersWaitingVotingEnd() {
        // desabilita a carta dos jogadores
        for (let i = 0; i < this.teamProjectOnline.length; i++) {
            let obj = this.teamProjectOnline[i];
            delete obj['valueVotedCard'];
            obj['typeCard'] = 'desabled';
        }

        // adiciona os jogadores que estavam aguardando
        for (const user of this.usersWaitingVotingEnd) {
            this.teamProjectOnline.push(user);
        }
    }

    // Envia uma notificação de alguma ação do usuário
    sendNotification(nameUser: string, action: string, nameStory?: string) {
        this.socket.emit('notification', {
            nameUser: nameUser,
            action: action,
            date: moment().format(),
            nameStory: nameStory
        });
    }

    // Abre as notificações
    openNotifications(event) {
        let popover = this.popoverCtrl.create('VotingProjectNotificationPage', {dataNotifications: this.dataNotifications});
        popover.present({ev: event});
        popover.onDidDismiss(() => {
            this.unreadNotifications = 0;
        });
    }

    // Abre as notificações
    openChat() {
        let modal = this.modalCtrl.create('VotingProjectChatPage', {
            dataChat: this.dataChat,
            userLogged: this.dataUserLogged,
            socket: this.socket,
            activeChat: this.activeChat,
            project: this.paramsProject,
            unreadNotifications: this.unreadNotifications,
            dataNotifications: this.dataNotifications
        });
        modal.present();
        modal.onDidDismiss((res) => {
            if (res) {
                this.dataChat = res.messages;
                this.unreadNotifications = res.unreadNotifications;
            }
            this.unreadChat = 0;
        });
    }

    // Método chamado no socket startVoting e restartVoting
    startVoting(story?: object) {
        let loading = this.loadingCtrl.create({content: 'Iniciando...'});
        loading.present();

        if (typeof story !== 'undefined') {
            this.storyCurrent = story[0];
        }

        if (this.idAdminProject == this.dataUserLogged._id) {
            this.storyService.deleteAllVotingStoryProject(this.paramsProject._id, this.storyCurrent._id).then(() => {
                loading.dismiss();
            }, (err) => {
                this.showToast("Não foi possível excluir as notas que foram aplicadas anteriormente nesta estória");
                loading.dismiss();
            });
        }

        // Inicia o cronometro
        this.segundos = 0;
        this.minutos = 0;
        this.horas = 0;
        var that = this;

        this.stopCronometro = setInterval(function () {
            that.cronometro();
        }, 1000); // 1000ms = 1s então 10ms = 0.01s

        this.activeChat = false;
        this.cardsTurn = false;
        this.votingStarted = true;
        this.textCardDesabled = "Aguardando usuário votar na estória";

        loading.dismiss();
    }

    // Mostra a página para escolher a estória a ser votada
    pageStartVoting() {
        if (this.teamProjectOnline.length <= 1) {
            return this.showAlert("Não permitido", "Para iniciar a votação, deve ter no mínimo 2 usuários online.");
        }

        let modal = this.modalCtrl.create('VotacaoProjetoEstoriasPage', {
            project: this.paramsProject,
            userLogged: this.dataUserLogged
        });
        modal.present();
        modal.onDidDismiss((res) => {
            if (res) {
                if (this.total_story_voted == 0) {
                    this.projectService.updateProject({start_in_date: moment().format()}, this.paramsProject._id)
                    .then(() => {
                        this.socket.emit('startVoting', res, this.socket.id);
                        this.sendNotification(this.dataUserLogged.name, "startVoting", res.story[0].title);
                    }, (err) => {
                        this.showToast("Não foi possível iniciar a votação.");
                    });
                } else {
                    this.socket.emit('startVoting', res, this.socket.id);
                    this.sendNotification(this.dataUserLogged.name, "startVoting", res.story[0].title);
                }
            }
        });
    }

    // Reinicia uma votação
    restartVotingStory() {
        this.socket.emit('restartVoting');
        this.sendNotification(this.dataUserLogged.name, "restartVoting", this.storyCurrent.title);
    }

    // Cancela a votação de uma estória quando ela já foi iniciada
    cancelVotingStarted() {
        let confirm = this.alertCtrl.create({
            title: 'Cancelar Votação',
            message: 'Deseja realmente cancelar a votação da estória atual?',
            buttons: [
                {text: 'Não'},
                {
                    text: 'Sim',
                    handler: () => {
                        var obj = {
                            current_story: null
                        };

                        this.projectService.updateProject(obj, this.paramsProject._id).then(() => {
                            this.socket.emit('cancelVoting');
                            this.sendNotification(this.dataUserLogged.name, "cancelVoting", this.storyCurrent.title);
                        }, (err) => {
                            console.log(err);
                        });
                    }
                }
            ]
        });
        confirm.present();
    }

    // Mostra a página para o usuário escolher a carta para votar
    openPageCards() {
        let cards: any[] = [];
        let cardsActive: any;
        for (const card of this.paramsProject.cards) {
            cardsActive = {
                number: card,
                typeCard: "active"
            }
            cards.push(cardsActive);
        }

        let h, m , s;
        if (this.horas < 10) { h="0" + this.horas; } else { h=this.horas; };
        if (this.minutos < 10) { m="0" + this.minutos; } else { m=this.minutos; };
        if (this.segundos < 10) { s="0" + this.segundos; } else { s=this.segundos; };

        let time_voting = h + ':' + m + ':' + s;

        let modal = this.modalCtrl.create('VotacaoCartasPage', {
            cards: cards,
            project: this.paramsProject,
            cardSelected: this.cardSelected,
        });
		modal.onDidDismiss((data) => {
            if (data) {
                let loading = this.loadingCtrl.create({content: 'Aguarde...'});
                loading.present();

                this.storyService.saveStoryVoting({note: data.cardSelected, time_voting: time_voting}, this.paramsProject._id, this.storyCurrent._id).then(() => {
                    this.cardSelected = data.cardSelected;
                    this.socket.emit('votedCard', data.cardSelected);
                    loading.dismiss();

                }, (err) => {
                    this.showToast("Não foi possível salvar a carta que você escolheu");
                    loading.dismiss();
                });
            }
		});
		modal.present();
    }

    turnCards() {
        let soma = 0;
        for (let i = 0; i < this.teamProjectOnline.length; i++) {
            if (typeof this.teamProjectOnline[i].valueVotedCard !== 'undefined' && this.teamProjectOnline[i].valueVotedCard !== null) {
                soma++;
            }
        }

        if (soma > 0) {
            if (soma < this.teamProjectOnline.length) {
                let alert = this.alertCtrl.create({
                    title: 'VIRAR CARTAS',
                    message: 'Ainda falta usuários para votar, deseja continuar?',
                    buttons: [
                    { text: 'Não' },
                    { text: 'Sim',
                        handler: () => {
                            this.socket.emit('turnCards');
                            this.sendNotification(this.dataUserLogged.name, "turnedCards");
                        }
                    }
                    ]
                });
                alert.present();
            } else {
                this.socket.emit('turnCards');
                this.sendNotification(this.dataUserLogged.name, "turnedCards");
            }
        } else {
            this.showAlert("Não permitido", "Para virar as cartas pelo menos 1 usuário tem que ter votado.");
        }
    }

    viewDescriptionStory() {
        this.showAlert(this.storyCurrent.title, this.storyCurrent.description);
    }

    setFinalCardStory() {
        let alert = this.alertCtrl.create({
            title: 'CARTA FINAL',
            message: "Informe o número de pontos para essa tarefa.",
            inputs: [{
                name: 'value',
                placeholder: 'Informe um valor',
                type: 'number'
            }],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                },
                {
                    text: 'Concluir',
                    handler: data => {
                        let loading = this.loadingCtrl.create({content: 'Aguarde...'});
                        loading.present();

                        let h, m, s;
                        if (this.horas < 10) {
                            h = "0" + this.horas;
                        } else {
                            h = this.horas;
                        };

                        if (this.minutos < 10) {
                            m = "0" + this.minutos;
                        } else {
                            m = this.minutos;
                        };
                        
                        if (this.segundos < 10) {
                            s = "0" + this.segundos;
                        } else {
                            s = this.segundos;
                        };

                        let time_total = h + ':' + m + ':' + s;

                        this.storyService.updateStory({
                            time_total: time_total,
                            card_final: data.value
                        }, this.paramsProject._id, this.storyCurrent._id).then((result) => {
                            var obj = {
                                current_story: null
                            };

                            if (this.total_story == this.total_story_voted) {
                                obj['date_end'] = moment().format();
                            }

                            this.projectService.updateProject(obj, this.paramsProject._id).then(() => {
                                loading.dismiss();
                                this.socket.emit('finalCardStory', data.value);
                                this.sendNotification(this.dataUserLogged.name, "finishVoting", this.storyCurrent.title);
                            }, (err) => {
                                this.showToast("Não foi possível salvar a Carta Final");
                                loading.dismiss();
                            });

                        }, (err) => {
                            this.showToast("Não foi possível salvar a Carta Final");
                            loading.dismiss();
                        });
                    }
                }
            ]
        });
        alert.present();
    }

    showToast(message: string, duration?: number) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            showCloseButton: true,
            closeButtonText: "Ok"
        });
        toast.present();
    }

    showAlert(title: string, subTitle: string){
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            buttons: ['OK']
        });
        alert.present();
    }
}
