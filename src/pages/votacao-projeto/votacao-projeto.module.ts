import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';

import {CartaComponentModule} from '../../components/carta/carta.module';
import {VotacaoProjetoPage} from './votacao-projeto';

@NgModule({
    declarations: [VotacaoProjetoPage],
    imports: [
        CartaComponentModule,
        IonicPageModule.forChild(VotacaoProjetoPage),
    ]
})
export class VotacaoProjetoPageModule {
}