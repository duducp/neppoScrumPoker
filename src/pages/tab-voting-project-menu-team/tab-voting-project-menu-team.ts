import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tab-voting-project-menu-team',
  templateUrl: 'tab-voting-project-menu-team.html',
})
export class TabVotingProjectMenuTeamPage {

  socket: any;
  paramsProject: any;
  team: any[] = [];

  constructor(public navParams: NavParams) {
    this.paramsProject = navParams.data.project;
    this.socket = navParams.data.socket;

    this.socket.on("getTeamOnline", (teamOnline) => {
      for(let i = 0; i < this.team.length; i++) {
        this.team[i]['online'] = false;
        for (const user of teamOnline) {
          if (user._id == this.team[i]._id) {
            this.team[i]['online'] = true;
          }
        }
      }
    });
  }

  ionViewDidLoad() {
    let user;
    for(let i = 0; i < this.paramsProject.team.length; i++) {
        user = this.paramsProject.team[i];
        user['online'] = false;
        this.team.unshift(user);
    }
  }

  ionViewDidEnter() {
    this.socket.emit('getTeamOnline');
  }

}
