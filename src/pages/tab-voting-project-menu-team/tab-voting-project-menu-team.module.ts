import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabVotingProjectMenuTeamPage } from './tab-voting-project-menu-team';

@NgModule({
  declarations: [
    TabVotingProjectMenuTeamPage,
  ],
  imports: [
    IonicPageModule.forChild(TabVotingProjectMenuTeamPage),
  ],
  exports: [
    TabVotingProjectMenuTeamPage
  ]
})
export class TabVotingProjectMenuTeamPageModule {}
