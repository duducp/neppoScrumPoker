import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {PopoverProjectsPage} from './popover-projects';

@NgModule({
    declarations: [
        PopoverProjectsPage,
    ],
    imports: [
        IonicPageModule.forChild(PopoverProjectsPage),
    ],
    exports: [
        PopoverProjectsPage
    ]
})
export class PopoverProjectsPageModule {
}
