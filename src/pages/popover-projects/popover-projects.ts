import {Component} from '@angular/core';
import {
    IonicPage,
    AlertController,
    NavController,
    NavParams,
    ToastController,
    ViewController,
    App
} from "ionic-angular";

import {ProjectsService} from "../../providers/projects/projectsService";

@IonicPage()
@Component({
    template: `
        <ion-content>
            <ion-list no-margin no-padding no-lines>
                <ion-item (click)="detailsProject()">
                    <ion-icon name="book" item-start></ion-icon>
                    Detalhes
                </ion-item>

                <ion-item (click)="deleteProject()" color="danger" *ngIf="userLogged.role === 'admin' || userLogged._id === project.id_user">
                    <ion-icon name="trash" item-start></ion-icon>
                    Excluir
                </ion-item>
            </ion-list>
        </ion-content>
    `
})
export class PopoverProjectsPage {
    project: any;
    userLogged: {} = {};

    constructor(public navParams: NavParams,
                public navCtrl: NavController,
                public toastCtrl: ToastController,
                public alertCtrl: AlertController,
                public projectService: ProjectsService,
                public appCtrl: App,
                private viewCtrl: ViewController) {
        this.project = this.navParams.get('project');
        this.userLogged = this.navParams.get('userLogged');
    }

    // Abre a página para editar um projeto
    detailsProject() {
        this.viewCtrl.dismiss();
        this.appCtrl.getRootNav().push('TabProjectPage', {project: this.project, userLogged: this.userLogged});
    }

    // Exclui um projeto
    deleteProject() {
        let alert = this.alertCtrl.create({
            title: 'Exclusão',
            message: 'Deseja realmente excluir o projeto selecionado?',
            buttons: [
                {text: 'Não'},
                {
                    text: 'Sim', handler: () => {
                    this.projectService.deleteProject(this.project._id).then(result => {
                        let data: any = result;
                        this.showToast(data.message, 3000);
                        this.viewCtrl.dismiss({type: "delete"});

                    }, (err) => {
                        let body = JSON.parse(err._body);
                        let message = body.message;
                        this.showToast(message);
                    });
                }
                }
            ]
        });
        alert.present();
    }

    // Método para chamar o Toast
    showToast(message: string, duration?: number) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            showCloseButton: true,
            closeButtonText: "Ok"
        });
        toast.present();
    }
}
