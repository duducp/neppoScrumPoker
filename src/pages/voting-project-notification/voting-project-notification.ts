import {Component} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';
import * as moment from 'moment';

@IonicPage()
@Component({
    selector: 'page-voting-project-notification',
    templateUrl: 'voting-project-notification.html',
})
export class VotingProjectNotificationPage {
    dataNotifications: any[] = [];
    newDataNotifications: any[] = [];

    constructor(public navParams: NavParams) {
        moment.locale('pt-BR');

        this.dataNotifications = this.navParams.get('dataNotifications');

        for (let i = 0; i < this.dataNotifications.length; i++) {
            this.newDataNotifications.push({
                nameUser: this.dataNotifications[i].nameUser,
                nameStory: this.dataNotifications[i].nameStory,
                action: this.dataNotifications[i].action,
                date: this.dataNotifications[i].date,
                dateFrom: moment(this.dataNotifications[i].date).fromNow()
            })
        }
    }
}
