import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {VotingProjectNotificationPage} from './voting-project-notification';

@NgModule({
    declarations: [
        VotingProjectNotificationPage,
    ],
    imports: [
        IonicPageModule.forChild(VotingProjectNotificationPage),
    ],
    exports: [
        VotingProjectNotificationPage
    ]
})
export class VotingProjectNotificationPageModule {
}
