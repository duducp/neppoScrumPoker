import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {TabReportProjectPage} from './tab-report-project';

@NgModule({
    declarations: [
        TabReportProjectPage,
    ],
    imports: [
        IonicPageModule.forChild(TabReportProjectPage),
    ],
    exports: [
        TabReportProjectPage
    ]
})
export class TabReportProjectPageModule {
}
