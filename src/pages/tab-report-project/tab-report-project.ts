import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-tab-report-project',
    templateUrl: 'tab-report-project.html',
})
export class TabReportProjectPage {
    params: Object;

    tabProjectStoryRoot = 'TabReportProjectStoryPage';
    tabProjectTeamRoot = 'TabReportProjectTeamPage';
    tabProjectChartRoot = 'TabReportProjectChartPage';

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.params = this.navParams.get('project');
    }
}
