import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {TabReportProjectChartPage} from './tab-report-project-chart';

@NgModule({
    declarations: [
        TabReportProjectChartPage,
    ],
    imports: [
        IonicPageModule.forChild(TabReportProjectChartPage),
    ],
    exports: [
        TabReportProjectChartPage
    ]
})
export class TabReportProjectChartPageModule {
}
