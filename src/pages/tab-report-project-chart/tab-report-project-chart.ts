import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, App} from 'ionic-angular';
import {Chart} from 'chart.js';

import {StoryService} from "../../providers/story/storyService";

@IonicPage()
@Component({
    selector: 'page-tab-report-project-chart',
    templateUrl: 'tab-report-project-chart.html',
})
export class TabReportProjectChartPage {
    @ViewChild('barCanvas') barCanvas;
    @ViewChild('doughnutCanvas') doughnutCanvas;

    barChart: any;
    doughnutChart: any;

    paramsProject: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public loadingCtrl: LoadingController,
                public appCtrl: App,
                public storyService: StoryService) {
        this.paramsProject = navParams.data;
    }


    ionViewDidLoad() {
        this.getAllStorysVoted();
        this.noteStories();
        this.timeVotingStories();
    }

    getAllStorysVoted() {
        let loader = this.loadingCtrl.create({
            content: "Gerando gráficos..."
        });
        loader.present();

        this.storyService.getAllStorysVoted('/' + this.paramsProject._id)
            .then(res => {
                let data: any = res;
                this.paramsProject = data[0];

                loader.dismissAll();
            })
            .catch((err) => {
                loader.dismissAll();
            });
    }

    noteStories() {
        this.barChart = new Chart(this.barCanvas.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }

        });
    }

    timeVotingStories() {
        this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

            type: 'doughnut',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }]
            }

        });
    }

    dismiss() {
        this.appCtrl.getRootNav().pop();
    }
}
