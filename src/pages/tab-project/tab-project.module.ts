import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {TabProjectPage} from './tab-project';

@NgModule({
    declarations: [
        TabProjectPage,
    ],
    imports: [
        IonicPageModule.forChild(TabProjectPage),
    ]
})
export class TabProjectPageModule {
}
