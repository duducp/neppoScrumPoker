import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

@Component({
    selector: 'page-tab-project',
    templateUrl: 'tab-project.html'
})
@IonicPage()
export class TabProjectPage {

    params: Object;
    userLogged: Object;
    indexTab: Number = 0;

    tabProjectRoot = 'TabProjectDetailPage';
    tabProjectStoryRoot = 'TabProjectStoriesPage';

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.indexTab = this.navParams.get('indexTab');
        this.params = {
            project: this.navParams.get('project'),
            userLogged: this.navParams.get('userLogged')
        };
    }

}
