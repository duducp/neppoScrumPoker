import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  ItemSliding,
  AlertController
} from 'ionic-angular';

import {StoryService} from "../../providers/story/storyService";

@IonicPage()
@Component({
  selector: 'page-tab-voting-project-menu-story',
  templateUrl: 'tab-voting-project-menu-story.html',
})
export class TabVotingProjectMenuStoryPage {
  socket: any;
  paramsProject: any;
  stories: any[] = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
              public storyService: StoryService) {
    this.paramsProject = navParams.data.project;
    this.socket = navParams.data.socket;
  }

  ionViewDidLoad() {
    this.getAllStorysVoted();
  }

  getAllStorysVoted() {
    let loader = this.loadingCtrl.create({
        content: "Aguarde..."
    });
    loader.present();

    this.storyService.getAllStorysVoted(this.paramsProject._id)
      .then(res => {
          let data: any = res;
          this.stories = data;
          loader.dismissAll();
      })
      .catch((err) => {
          this.stories = [];
          loader.dismissAll();
      });
  }

  detailStory(story, slidingItem: ItemSliding) {
    let modal = this.modalCtrl.create('VotingProjectReportStoryPage', {
        project: this.paramsProject,
        story: story
    });
    modal.present();
    modal.onDidDismiss(() => {
        slidingItem.close();
    });
  }

  viewDescriptionStory(story, slidingItem: ItemSliding) {
    let alert = this.alertCtrl.create({
      title: story.title,
      subTitle: story.description,
      buttons: ['Ok']
    });
    alert.present();
    alert.onDidDismiss(() => {
        slidingItem.close();
    });
  }

  summaryStory(story, slidingItem: ItemSliding) {
    let modal = this.modalCtrl.create('VotingProjectReportStoryPage', {
        project: this.paramsProject,
        story: story
    });
    modal.present();
    modal.onDidDismiss(() => {
        slidingItem.close();
    });
  }
}
