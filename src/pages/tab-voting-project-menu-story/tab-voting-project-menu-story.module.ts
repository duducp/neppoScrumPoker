import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabVotingProjectMenuStoryPage } from './tab-voting-project-menu-story';

@NgModule({
  declarations: [
    TabVotingProjectMenuStoryPage,
  ],
  imports: [
    IonicPageModule.forChild(TabVotingProjectMenuStoryPage),
  ],
  exports: [
    TabVotingProjectMenuStoryPage
  ]
})
export class TabVotingProjectMenuStoryPageModule {}
