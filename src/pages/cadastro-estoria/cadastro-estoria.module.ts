import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';

import {CadastroEstoriaPage} from './cadastro-estoria';

@NgModule({
    declarations: [CadastroEstoriaPage],
    imports: [
        IonicPageModule.forChild(CadastroEstoriaPage),
    ]
})
export class CadastroEstoriaPageModule {
}