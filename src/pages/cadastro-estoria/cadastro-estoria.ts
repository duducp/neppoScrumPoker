import {Component, Input} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {
    IonicPage,
	NavParams,
	MenuController,
	ViewController,
	ToastController,
	LoadingController
} from 'ionic-angular';

import{StoryService} from '../../providers/story/storyService';

@IonicPage()
@Component({
	selector: 'page-cadastro-estoria',
	templateUrl: 'cadastro-estoria.html',
})
export class CadastroEstoriaPage {
	// Utilizado para inserir valores a um input
	@Input() title: string;
	@Input() description: string;

	//Variáveis
	form: FormGroup;
	loading: any;

	idProject: string;
	data: any; // Irá guardar o retorno da API
	tipo: string; // Irá guardar o valor que indica se estamos atualizando ou cadastrando
	textTitlePage: string = "Nova Estória";

	constructor(public navParams: NavParams,
                public fb: FormBuilder,
                public menuCtrl: MenuController,
                public viewCtrl: ViewController,
                public toastCtrl: ToastController,
                public loadingCtrl: LoadingController,
                public storyService: StoryService) {

		// Desabilita o MenuToggle
		menuCtrl.enable(false);

		// Válida os campos no form
		this.form = fb.group({
			'title': [null, Validators.required],
			'description': [null, Validators.required]
		});

		// Pega o id do projeto que a estória pertence
		this.idProject = this.navParams.get('idProject');

		// Grava se irá cadastrar ou editar
		this.tipo = this.navParams.get('tipo');

		// Chama o método
		this.checkEditStory();
	}

	// Atualiza a estória
	updateStory() {
		this.showLoader();

		this.storyService.updateStory(this.form.value, this.idProject, this.data._id).then((result) => {
			this.data = result;
			this.loading.dismiss();
			this.showToast(this.data.message, 3000);
			this.viewCtrl.dismiss(this.data.story);

		}, (err) => {
			let body = JSON.parse(err._body);
			let message = body.message;
			this.showToast(message);
			this.loading.dismiss();
		});
	}

	// Cadastra nova estória
	createStory() {
		this.showLoader();

		this.storyService.saveStory(this.form.value, this.idProject).then((result) => {
			this.data = result;
			this.loading.dismiss();
			this.showToast(this.data.message, 3000);
			this.viewCtrl.dismiss(this.data.story);

		}, (err) => {
			let body = JSON.parse(err._body);
			let message = body.message;
			this.showToast(message);
			this.loading.dismiss();
		});
	}


	// Verifica se está editando uma estória
	checkEditStory() {
		if (this.tipo === 'update') {
			this.data = this.navParams.get('story');

			// Preenche os inputs com os dados corretos
			this.title = this.data.title;
			this.description = this.data.description;

			// Informa ao validator quais os dados que os inputs tem
			this.form.setValue({
				title: this.data.title,
				description: this.data.description
			});

			this.textTitlePage = "Atualizar Estória";
		}
	}

	// Verifica o tipo do Submit
	submit() {
		if (this.tipo === 'update') {
			this.updateStory();
		} else {
			this.createStory();
		}
	}

	//Fecha o popup
	dismiss() {
		this.viewCtrl.dismiss();
	}

	// Mostra a tela de carregamento
	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: 'Aguarde...'
		});

		this.loading.present();
	}

	// Método para chamar o Toast
	showToast(message: string, duration?: number) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: duration,
			showCloseButton: true,
			closeButtonText: "Ok"
		});
		toast.present();
	}
}
