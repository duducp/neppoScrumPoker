import {Component} from '@angular/core';
import {
    IonicPage,
    LoadingController,
    MenuController,
    NavController,
    PopoverController,
    ToastController,
} from 'ionic-angular';

import {ProjectsService} from "../../providers/projects/projectsService";
import {AuthService} from '../../providers/auth/authService';

@IonicPage()
@Component({
    selector: 'page-projetos',
    templateUrl: 'projetos.html',
    providers: [ProjectsService]
})
export class ProjetosPage {

    projects: any[] = [];
    dataUserLogged: any = {};

    constructor(public navCtrl: NavController,
                public menuCtrl: MenuController,
                public toastCtrl: ToastController,
                public loadingCtrl: LoadingController,
                public popoverCtrl: PopoverController,
                public projectService: ProjectsService,
                public authService: AuthService) {
    }

    // Ação que acontece quando a página está prestes a ser exibida
    ionViewWillEnter() {
        this.getUserLogged();
        this.getAllProjects();
    }

    // Busca os projetos no banco de dados
    getAllProjects() {
        let loader = this.loadingCtrl.create({
            content: "Carregando projetos..."
        });
        loader.present();

        this.projectService.getAllProjects()
            .then(res => {
                let project: any = res;
                this.projects = project;
                loader.dismissAll();

            })
            .catch(err => {
                if (err.url) {
                    let body = JSON.parse(err._body);
                    let message = body.message;
                    this.showToast(message, 5000);
                    loader.dismissAll();
                } else {
                    this.showToast("Ops... Tivemos problemas ao comunicar com o servidor!", 5000);
                    loader.dismissAll();
                }
            });
    }

    openProject(project) {
        if (!project.date_end) {
            let loader = this.loadingCtrl.create({
                content: "Aguarde..."
            });
            loader.present();

            this.projectService.getProject(project._id)
            .then(data => {
                let dataProject = JSON.parse(JSON.stringify(data));

                let obj = {
                    total_story: project.total_story,
                    total_story_voted: project.total_story_voted,
                };
                obj['project'] = dataProject[0];

                // Verifica se o usuário logado pertence a equipe do projeto
                this.dataUserLogged['isTeam'] = false;
                for(let i = 0; i < dataProject[0].team.length; i++) {
                    if(dataProject[0].team[i]._id == this.dataUserLogged._id) {
                        this.dataUserLogged['isTeam'] = true;
                        break;
                    }
                }

                this.navCtrl.setRoot('VotacaoProjetoPage', {project: obj, userLogged: this.dataUserLogged});
                loader.dismissAll();

            }, err => {
                this.showToast("Ops... Tivemos problemas ao comunicar com o servidor!", 5000);
                loader.dismissAll();
            })
            .catch(err => {
                let body = JSON.parse(err._body);
                let message = body.message;
                this.showToast(message, 3000);
                loader.dismissAll();
            });
        } else {
            this.navCtrl.push('TabReportProjectPage', {project: project});
        }
    }

    // Mostra a página de cadastro de projeto
    cadastroProjeto() {
        this.navCtrl.push('CadastroProjetoPage', {userLogged: this.dataUserLogged});
    }

    // Popover de menu de cada projeto
    projectPopover(myEvent, project, index) {
        let popover = this.popoverCtrl.create('PopoverProjectsPage', {
            project: project,
            userLogged: this.dataUserLogged
        });
        popover.present({
            ev: myEvent
        });

        // Ação que executa após o popover fechar
        popover.onDidDismiss((res) => {
            if (res) {
                if (res.type == 'update') {
                    this.projects.splice(index, 1, res.project);
                } else if (res.type == 'delete') {
                    this.projects.splice(index, 1);
                }
            }
        });
    }

    // Pega os dados do usuário logado
    getUserLogged() {
        this.authService.getUserLogged()
            .then(data => {
                this.dataUserLogged = data;
            })
            .catch(err => {
                console.log({erro: err});
            })
    }

    openMenu() {
        this.menuCtrl.toggle();
    }

    // Atualizar página
    doRefresh(refresher) {
        setTimeout(() => {
            this.getAllProjects();
            refresher.complete();
        }, 2000);
    }

    // Método para chamar o Toast
    showToast(message: string, duration?: number) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            showCloseButton: true,
            closeButtonText: "Ok"
        });
        toast.present();
    }
}
