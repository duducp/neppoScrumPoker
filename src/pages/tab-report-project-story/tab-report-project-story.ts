import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, App, LoadingController, AlertController} from 'ionic-angular';

import {StoryService} from "../../providers/story/storyService";

@IonicPage()
@Component({
    selector: 'page-tab-report-project-story',
    templateUrl: 'tab-report-project-story.html',
})
export class TabReportProjectStoryPage {

    paramsProject: any;
    stories: any[] = [];

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public appCtrl: App,
                public loadingCtrl: LoadingController,
                public alertCtrl: AlertController,
                public storyService: StoryService) {
        this.paramsProject = navParams.data;
    }

    ionViewDidLoad() {
        this.getAllStorysVoted();
    }

    getAllStorysVoted() {
        let loader = this.loadingCtrl.create({
            content: "Gerando relatório..."
        });
        loader.present();

        this.storyService.getAllStorysVoted(this.paramsProject._id)
            .then(res => {
                let data: any = res;
                this.stories = data;

                loader.dismissAll();
            })
            .catch((err) => {
                this.stories = [];
                loader.dismissAll();
            });
    }

    dismiss() {
        this.appCtrl.getRootNav().pop();
    }

    doRefresh(refresher) {
        setTimeout(() => {
            this.getAllStorysVoted();
            refresher.complete();
        }, 2000);
    }

    viewDescriptionProject() {
        this.showAlert(this.paramsProject.title, this.paramsProject.description);
    }

    showAlert(title: string, subTitle: string) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            buttons: ['OK']
        });
        alert.present();
    }
}
