import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {TabReportProjectStoryPage} from './tab-report-project-story';

@NgModule({
    declarations: [
        TabReportProjectStoryPage,
    ],
    imports: [
        IonicPageModule.forChild(TabReportProjectStoryPage),
    ],
    exports: [
        TabReportProjectStoryPage
    ]
})
export class TabReportProjectStoryPageModule {
}
