import {Component} from '@angular/core';
import {
    IonicPage,
    App,
    AlertController,
    LoadingController,
    ModalController,
    NavController,
    NavParams,
    ToastController,
    ItemSliding
} from 'ionic-angular';

import {StoryService} from "../../providers/story/storyService";
import {AuthService} from '../../providers/auth/authService';

@IonicPage()
@Component({
    selector: 'page-tab-project-stories',
    templateUrl: 'tab-project-stories.html',
})
export class TabProjectStoriesPage {
    paramsProject: any;
    storys: any[] = [];
    dataUserLogged: {} = {};

    constructor(public navCtrl: NavController,
                public appCtrl: App,
                public modalCtrl: ModalController,
                public params: NavParams,
                public toastCtrl: ToastController,
                public loadingCtrl: LoadingController,
                public alertCtrl: AlertController,
                public storyService: StoryService,
                public authService: AuthService) {

        // Pega os dados do projeto
        this.paramsProject = params.data.project;
    }

    ionViewDidLoad() {
        this.getUserLogged();
        this.getAllStoryes();
    }

    insertStory() {
        let modal = this.modalCtrl.create('CadastroEstoriaPage', {idProject: this.paramsProject._id});
        modal.present();
        modal.onDidDismiss((res) => {
            if (res) {
                this.storys.push(res);
            }
        });
    }

    updateStory(data: any, index) {
        let modal = this.modalCtrl.create('CadastroEstoriaPage', {
            idProject: this.paramsProject._id, story: data, tipo: 'update'
        });
        modal.present();
        modal.onDidDismiss((res) => {
            if (res) {
                this.storys.splice(index, 1, res);
            }
        });
    }

    deleteStory(story: any, slidingItem: ItemSliding, index) {
        let alert = this.alertCtrl.create({
            title: 'Exclusão',
            message: 'Deseja realmente excluir a estória selecionada?',
            buttons: [
                {
                    text: 'Não', handler: () => {
                    slidingItem.close();
                }
                }, {
                    text: 'Sim', handler: () => {
                        this.storyService.deleteStory(this.paramsProject._id, story._id)
                            .then(result => {
                                let data: any = result;
                                this.storys.splice(index, 1);
                                this.showToast(data.message, 3000);

                            }, (err) => {
                                let body = JSON.parse(err._body);
                                let message = body.message;
                                this.showToast(message);
                            });
                    }
                }
            ]
        });
        alert.present();
    }

    // Busca todas as estórias do projeto
    getAllStoryes() {
        let loader = this.loadingCtrl.create({
            content: "Carregando estórias..."
        });
        loader.present();

        this.storyService.getAllStoryProject(this.paramsProject._id)
            .then(res => {
                let story: any = res;
                this.storys = story;
                loader.dismissAll();
            })
            .catch((err) => {
                this.storys = [];
                loader.dismissAll();
            });
    }

    // Pega os dados do usuário logado
    getUserLogged() {
        this.authService.getUserLogged()
            .then(data => {
                this.dataUserLogged = data;
            })
            .catch(err => {
                console.log({erro: err});
            })
    }

    doRefresh(refresher) {
        setTimeout(() => {
            this.getAllStoryes();
            refresher.complete();
        }, 2000);
    }

    // Botão de voltar da página
    dismiss() {
        this.appCtrl.getRootNav().pop();
    }

    showToast(message: string, duration?: number) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            showCloseButton: true,
            closeButtonText: "Ok"
        });
        toast.present();
    }
}
