import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {TabProjectStoriesPage} from './tab-project-stories';

@NgModule({
    declarations: [
        TabProjectStoriesPage,
    ],
    imports: [
        IonicPageModule.forChild(TabProjectStoriesPage),
    ],
    exports: [
        TabProjectStoriesPage
    ]
})
export class TabProjectStoriesPageModule {
}
