import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, PopoverController, App } from 'ionic-angular';

@Component({
  selector: 'page-tab-voting-project-menu',
  templateUrl: 'tab-voting-project-menu.html'
})
@IonicPage()
export class TabVotingProjectMenuPage {

  tabVotingProjectMenuTeamRoot = 'TabVotingProjectMenuTeamPage'
  tabVotingProjectMenuStoryRoot = 'TabVotingProjectMenuStoryPage'

  params: any;
  socket: any;

  // Notifications
  unreadNotifications: number;
  dataNotifications: any[] = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public alertCtrl: AlertController,
              public appCtrl: App,
              public popoverCtrl: PopoverController) {        
    this.params = {
        project: this.navParams.get('project'),
        userLogged: this.navParams.get('userLogged'),
        socket: this.navParams.get('socket'),
        unreadNotifications: this.navParams.get('unreadNotifications'),
        dataNotifications: this.navParams.get('dataNotifications')
    };

    this.socket = this.params.socket;
    this.dataNotifications = this.params.dataNotifications;
    this.unreadNotifications = this.params.unreadNotifications;

    // Informa sobre nova notificação
    this.socket.on("notification", (data) => {
        this.unreadNotifications = this.unreadNotifications + 1;
    })
  }

  // Abre as notificações
  openNotifications(event) {
      let popover = this.popoverCtrl.create('VotingProjectNotificationPage', {dataNotifications: this.dataNotifications});
      popover.present({ev: event});
      popover.onDidDismiss(() => {
          this.unreadNotifications = 0;
      });
  }

  exitProject() {
    let alert = this.alertCtrl.create({
      title: 'SAIR DO PROJETO',
      message: 'Deseja realmente sair do projeto?',
      buttons: [
        {
          text: 'Não'
        },
        {
          text: 'Sim',
          handler: () => {
            this.socket.emit('userDesconnected');
            this.viewCtrl.dismiss();
            this.appCtrl.getRootNav().setRoot('ProjetosPage');
          }
        }
      ]
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.unreadNotifications);
  }

}
