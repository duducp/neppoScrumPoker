import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabVotingProjectMenuPage } from './tab-voting-project-menu';

@NgModule({
  declarations: [
    TabVotingProjectMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(TabVotingProjectMenuPage),
  ]
})
export class TabVotingProjectMenuPageModule {}
