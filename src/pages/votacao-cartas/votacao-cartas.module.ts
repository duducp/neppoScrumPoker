import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';

import {CartaComponentModule} from '../../components/carta/carta.module';
import {VotacaoCartasPage} from './votacao-cartas';

@NgModule({
    declarations: [VotacaoCartasPage],
    imports: [
        CartaComponentModule,
        IonicPageModule.forChild(VotacaoCartasPage),
    ]
})
export class VotacaoCartasPageModule {
}