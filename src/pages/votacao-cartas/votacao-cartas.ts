import {Component} from '@angular/core';
import { IonicPage, NavParams, ViewController, ToastController, LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-votacao-cartas',
	templateUrl: 'votacao-cartas.html',
})
export class VotacaoCartasPage {
    paramsProject: any;
	titleProject: string;
	cardsProject: any;
    cardSelected: number;
    numCardSelectLeft: any;

    constructor(public navParams: NavParams,
                public toastCtrl: ToastController,
                public loadingCtrl: LoadingController,
                public viewCtrl: ViewController) {
		// Pega os dados do projeto
        this.paramsProject = this.navParams.get('project');
        this.titleProject = this.paramsProject.title;
        this.cardsProject = this.navParams.get('cards');
		this.cardSelected = this.navParams.get('cardSelected');
	}

    ionViewDidLoad(){
        if (this.cardSelected) {
            let card = {
                number: this.cardSelected,
                typeCard: "select"
            };

            for (let i = 0; i < this.cardsProject.length; i++){
                if (this.cardsProject[i].number == this.cardSelected){
                    this.cardsProject.splice(i, 1, card);

                    this.numCardSelectLeft = {
                        index: i,
                        number: card.number
                    };
                    break;
                }
            }
        }
    }

    selectCard(card, index) {
        // Verifica qual foi a carta selecionada anteriormente e desmarca ela
        if (this.numCardSelectLeft) {
            let card = {
                number: this.numCardSelectLeft.number,
                typeCard: "active"
            };
            this.cardsProject.splice(this.numCardSelectLeft.index, 1, card);
        }

        let newCard = {
            number: card.number,
            typeCard: "select"
        };

        this.numCardSelectLeft = {
            index: index,
            number: card.number
        };

        this.cardsProject.splice(index, 1, newCard); // Marca a nova carta selecionada
        this.cardSelected = card.number;
	}

	saveCard(){
        if (!this.cardSelected) {
            this.showToast("Você deve selecionar uma carta para continuar", 3000);
        } else {
            this.viewCtrl.dismiss({cardSelected: this.cardSelected});
        }
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}

    showToast(message: string, duration?: number) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            showCloseButton: true,
            closeButtonText: "Ok"
        });
        toast.present();
    }
}
