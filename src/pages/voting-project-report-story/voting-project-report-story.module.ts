import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VotingProjectReportStoryPage } from './voting-project-report-story';

@NgModule({
  declarations: [
    VotingProjectReportStoryPage,
  ],
  imports: [
    IonicPageModule.forChild(VotingProjectReportStoryPage),
  ],
  exports: [
    VotingProjectReportStoryPage
  ]
})
export class VotingProjectReportStoryPageModule {}
