import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, App, ViewController } from 'ionic-angular';
import {Chart} from 'chart.js';

import { StoryService } from './../../providers/story/storyService';

@IonicPage()
@Component({
  selector: 'page-voting-project-report-story',
  templateUrl: 'voting-project-report-story.html',
})
export class VotingProjectReportStoryPage {
    paramsProject: any;
    story: any;
    data: any;

    totalUsersVoted: number;
    mediaNotas: number;
    pizzaChart: any;
    @ViewChild('pizzaChartCanvas') pizzaChartCanvas;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public appCtrl: App,
                public loadingCtrl: LoadingController,
                public viewCtrl: ViewController,
                public storyService: StoryService,) {
        this.paramsProject = navParams.get('project');
        this.story = navParams.get('story');
    }

    ionViewDidLoad() {
        this.getStoryVoted();
    }

    getStoryVoted() {
        let loader = this.loadingCtrl.create({
            content: "Gerando relatório..."
        });
        loader.present();

        this.storyService.getAllStorysVoted(this.paramsProject._id + '/?key=_id&text=' + this.story._id)
            .then(res => {
                let data: any = res;
                this.data = data[0];

            // INICIO GERAÇÃO DADOS PARA O GRÁFICO
                // Separo somente as notas dos usuários
                let notes = [];
                for(let i = 0; i < this.data.users_voted.length; i++) {
                    notes.push(this.data.users_voted[i].note)
                }
                notes.sort();
                
                // Verifico quantas vezes cada nota foi votada
                let notesRepeated = [];
                let qtd = 0;
                let index = 0;
                let somaNotes = 0;
                for(let i = 0; i < notes.length; i++) {
                    if (notes[i] == notes[i - 1] && i > 0) {
                        qtd = qtd + 1;
                        let d = {
                            note: notes[i],
                            qtd: qtd
                        };
                        notesRepeated.splice(index, 1, d)
                    } else {
                        qtd = 1;
                        index = i;
                        let d = {
                            note: notes[i],
                            qtd: qtd
                        };
                        notesRepeated.push(d);
                    }
                    if (!isNaN(parseFloat(notes[i])) && isFinite(notes[i])) {
                        somaNotes = somaNotes + JSON.parse(notes[i]);
                    }
                }

                // Calculo a média de votação em cada nota
                let porcentagem = [];
                for(let i = 0; i < notesRepeated.length; i++) {
                    let m = (notesRepeated[i].qtd * 100) / this.data.users_voted.length;
                    porcentagem.push(m.toFixed(2));
                }

                // Removo as notas repetidas do array
                var notesIndividual = notes.filter(function(este, i) {
                    return notes.indexOf(este) == i;
                })

                let notesIndividualText = [];
                for(let i = 0; i < notesIndividual.length; i++) {
                    for(let n = 0; n < notesRepeated.length; n++) {
                        if (notesRepeated[n].note == notesIndividual[i]) {
                            let text;
                            if (notesRepeated[n].qtd > 1) {
                                text = ' votos)';
                            } else {
                                text = ' voto)';
                            }
                            let m = 'Nota ' + notesIndividual[i] + ' (' + notesRepeated[n].qtd + text;
                            notesIndividualText.push(m);
                            break;
                        }
                    }
                }

                this.totalUsersVoted = this.data.users_voted.length;
                this.mediaNotas = Number((somaNotes / this.totalUsersVoted).toFixed(2));
                this.chart(notesIndividualText, porcentagem);
            // FIM GERAÇÃO DADOS PARA O GRÁFICO

                loader.dismissAll();                
            })
            .catch((err) => {
                console.log(err)
                this.data = [];
                loader.dismissAll();
            });
    }

    chart(labels, data) {
        this.pizzaChart = new Chart(this.pizzaChartCanvas.nativeElement, {

            type: 'doughnut',
            data: {
                labels: labels,
                datasets: [{
                    label: '# of Votes',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(32, 178, 170, 0.2)',
                        'rgba(112, 128, 144, 0.2)',
                        'rgba(47, 79, 79, 0.2)',
                        'rgba(139, 0, 139, 0.2)',
                        'rgba(50, 205, 50, 0.2)',
                        'rgba(139, 69, 19, 0.2)',
                        'rgba(184, 134, 11, 0.2)'
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        "#20B2AA",
                        "#708090",
                        "#2F4F4F",
                        "#8B008B",
                        "#8B4513",
                        "#B8860B"
                    ]
                }]
            }

        });
    }

    //Fecha o popup
	dismiss() {
		this.viewCtrl.dismiss();
	}
}
