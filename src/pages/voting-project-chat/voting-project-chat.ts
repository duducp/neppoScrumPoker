import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavParams, ViewController, PopoverController, Content } from 'ionic-angular';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-voting-project-chat',
  templateUrl: 'voting-project-chat.html',
})
export class VotingProjectChatPage {
  @ViewChild('txtChat') txtChat: any;
  @ViewChild(Content) content: Content;
  
  messages: any[] = [];
  dataChat: any[] = [];
  activeChat: boolean;

  // Notifications
  unreadNotifications: number;
  dataNotifications: any[] = [];

  socket: any;
  userLogged: any;
  project: any;

  constructor(public navParams: NavParams, private _zone: NgZone, public viewCtrl: ViewController, public popoverCtrl: PopoverController) {
    moment.locale('pt-BR');
    
    this.activeChat = navParams.data.activeChat;
    this.dataChat = navParams.data.dataChat ? navParams.data.dataChat : [];

    this.userLogged = navParams.data.userLogged;
    this.project = navParams.data.project;
    this.socket = navParams.data.socket;

    this.unreadNotifications = navParams.data.unreadNotifications;
    this.dataNotifications = navParams.data.dataNotifications;

    // Mostra as mensagens no Chat
    for (let i = 0; i < this.dataChat.length; i++) {
      this.addMessage(this.dataChat[i]);
    }

    // Se o chat está bloqueado, irá bloquear somente para o usuário que pertence a equipe do projeto
    if (!this.activeChat) {
      this.activeChat = true;
      for (let i = 0; i < this.project.team.length; i++) {
          if (this.project.team[i]._id == this.userLogged._id) {
            this.activeChat = false;
            break;
          }
      }
    }

    // Recebe as novas mensagens
    this.socket.on("messages", (data) => {
      this.addMessage(data);
    })

    // Informa sobre nova notificação
    this.socket.on("notification", (data) => {
        this.unreadNotifications = this.unreadNotifications + 1;
    })

    this.socket.emit('enterExitConversation', {nameUser: this.userLogged.name, action: 'enter'});
    this.socket.on("enterExitConversation", (data) => {
        this.addMessage(data);
    })
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.txtChat.setFocus();
    }, 150);
  }

  // Executa antes que a página possa ser destruída
  ionViewCanLeave() {
    this.dismiss();
  }

  public sendMessage() {
    this.txtChat.setFocus();

    if (!this.txtChat.content) {
      return;
    }

    let msg = {
      nameUser: this.userLogged.name,
      message: this.txtChat.content,
      date: moment().format(),
    }

    this.socket.emit('messages', msg);
    msg['isMe'] = true;

    this.addMessage(msg);
    this.txtChat.clearInput();
  }

  public addMessage(msg) {
    this._zone.run(() => {
      this.messages.push(msg);

      setTimeout(() => {
        this.content.scrollToBottom(300);//300ms animation speed
      });
    });
  }

    // Abre as notificações
  public openNotifications(event) {
      let popover = this.popoverCtrl.create('VotingProjectNotificationPage', {dataNotifications: this.dataNotifications});
      popover.present({ev: event});
      popover.onDidDismiss(() => {
          this.unreadNotifications = 0;
      });
  }

  public dismiss() {
    this.socket.emit('enterExitConversation', {nameUser: this.userLogged.name, action: 'exit'});
    this.viewCtrl.dismiss({messages: this.messages, unreadNotifications: this.unreadNotifications});
  }
}
