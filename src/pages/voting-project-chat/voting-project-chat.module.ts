import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ChatComponentModule } from '../../components/chat/chat.module';
import { ElasticTextAreaComponentModule } from './../../components/elastic-text-area/elastic-text-area.module';
import { VotingProjectChatPage } from './voting-project-chat';

@NgModule({
  declarations: [
    VotingProjectChatPage,
  ],
  imports: [
    ChatComponentModule,
    ElasticTextAreaComponentModule,
    IonicPageModule.forChild(VotingProjectChatPage),
  ],
  exports: [
    VotingProjectChatPage
  ]
})
export class VotingProjectChatPageModule {}
