import {Component} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    App,
    ToastController,
    LoadingController,
    ModalController
} from 'ionic-angular';

import {ProjectsService} from "../../providers/projects/projectsService";

@IonicPage()
@Component({
    selector: 'page-tab-project-detail',
    templateUrl: 'tab-project-detail.html',
})
export class TabProjectDetailPage {
    paramsProject: any;
    userLogged: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public toastCtrl: ToastController,
                public loadingCtrl: LoadingController,
                public appCtrl: App,
                public modalCtrl: ModalController,
                public projectService: ProjectsService) {

        this.userLogged = this.navParams.data.userLogged;
        this.getProject();
    }

    getProject() {
        let loader = this.loadingCtrl.create({
            content: "Aguarde..."
        });
        loader.present();

        this.projectService.getProject(this.navParams.data.project._id)
            .then(res => {
                let data: any = res;
                this.paramsProject = data[0];
                loader.dismissAll();

            }, err => {
                this.showToast("Ops... Tivemos problemas ao comunicar com o servidor!", 5000);
                loader.dismissAll();
            })
            .catch(err => {
                let body = JSON.parse(err._body);
                let message = body.message;
                this.showToast(message, 3000);
                loader.dismissAll();
            });
    }

    editProject() {
        let modal = this.modalCtrl.create('CadastroProjetoPage', {project: this.navParams.data.project, type: "edit"});
        modal.present();
        modal.onDidDismiss(() => {
            this.getProject();
        });
    }

    dismiss() {
        this.appCtrl.getRootNav().pop();
    }

    // Método para chamar o Toast
    showToast(message: string, duration?: number) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            showCloseButton: true,
            closeButtonText: "Ok"
        });
        toast.present();
    }
}
