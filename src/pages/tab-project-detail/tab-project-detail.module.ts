import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {TabProjectDetailPage} from './tab-project-detail';

@NgModule({
    declarations: [
        TabProjectDetailPage,
    ],
    imports: [
        IonicPageModule.forChild(TabProjectDetailPage),
    ],
    exports: [
        TabProjectDetailPage
    ]
})
export class TabProjectDetailPageModule {
}
